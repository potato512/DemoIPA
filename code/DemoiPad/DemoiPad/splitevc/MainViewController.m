//
//  MainViewController.m
//  DemoiPad
//
//  Created by zhangshaoyu on 2018/7/11.
//  Copyright © 2018年 zhangshaoyu. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableview;
@property (nonatomic, strong) NSArray *array;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"主视图";

    [self setUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor greenColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
}

- (void)setUI
{
    self.tableview = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:self.tableview];
    self.tableview.backgroundColor = [UIColor greenColor];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    //
    UIImageView *headerview = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 80.0, 80.0)];
    headerview.contentMode = UIViewContentModeScaleAspectFit;
    headerview.image = [UIImage imageNamed:@"header"];
    self.tableview.tableHeaderView = headerview;
    //
    [self.tableview reloadData];
}

- (NSArray *)array
{
    if (_array == nil) {
        _array = @[@"奔弛汽车", @"本田CRV汽车", @"北京现代达观汽车", @"雷克萨斯汽车"];
    }
    return _array;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, (cell.contentView.frame.size.width - 10.0), cell.contentView.frame.size.height)];
        [cell.contentView addSubview:label];
        label.tag = 10000;
        label.textColor = [UIColor blackColor];
    }
    
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:10000];
    label.text = self.array[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"cell click %ld", indexPath.row);
    if (self.itemClick) {
        self.itemClick(indexPath.row);
    }
}

@end
