//
//  BeiChiViewController.m
//  DemoiPad
//
//  Created by zhangshaoyu on 2018/7/11.
//  Copyright © 2018年 zhangshaoyu. All rights reserved.
//

/*
 https://www.jianshu.com/p/6ac34ab1ea24
 
 */

#import "BeiChiViewController.h"
#import "MenuViewController.h"

@interface BeiChiViewController () <UIPopoverControllerDelegate>

@end

@implementation BeiChiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"奔弛汽车";
    
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"showItem" style:UIBarButtonItemStyleDone target:self action:@selector(itemClick)];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"showView" style:UIBarButtonItemStyleDone target:self action:@selector(viewClick)];
    self.navigationItem.rightBarButtonItems = @[item1, item2];
    
    CGFloat width = (self.view.frame.size.width - 200.0 * 2);
    CGFloat height = (self.view.frame.size.height - 200.0 * 2);
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(200.0, 200.0, width, height)];
    [self.view addSubview:imageview];
    imageview.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    imageview.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.3];
    imageview.image = [UIImage imageNamed:@"BeiChi"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"%@,%@", self.view, self.view.subviews);
}

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor blueColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
}

- (void)itemClick
{
    // 1 设置内容控制器
    MenuViewController *controller = [[MenuViewController alloc] init];
    controller.preferredContentSize = CGSizeMake(200, 200);
    controller.modalPresentationStyle = UIModalPresentationPopover;
    
    // 2 出现
    // 2-1 出现在UIBarButtonItem上面的
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.barButtonItem = self.navigationItem.rightBarButtonItem;
    
    // 3 显示
    [self presentViewController:controller animated:YES completion:nil];
    
    // 4
    controller.itemClick = ^(NSInteger index) {
        
        // 5 消失
        [self dismissViewControllerAnimated:YES completion:NULL];
        
        // 6
        MenuViewController *nextVC = [MenuViewController new];
        [self.navigationController pushViewController:nextVC animated:YES];
    };
}

- (void)viewClick
{
    // 1 设置内容控制器
    MenuViewController *controller = [[MenuViewController alloc] init];
    controller.preferredContentSize = CGSizeMake(200, 200);
    controller.modalPresentationStyle = UIModalPresentationPopover;
    
    // 2 出现
    // 2-2 出现在view上面的
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.sourceView = self.view;
    popController.sourceRect = self.view.bounds;
    popController.permittedArrowDirections = UIPopoverArrowDirectionRight;
    
    // 3 显示
    [self presentViewController:controller animated:YES completion:nil];
    
    // 4
    controller.itemClick = ^(NSInteger index) {
        
        // 5 消失
        [self dismissViewControllerAnimated:YES completion:NULL];
        
        // 6
        MenuViewController *nextVC = [MenuViewController new];
        [self.navigationController pushViewController:nextVC animated:YES];
    };
}

-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    // 适配
    return UIModalPresentationNone;
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    return YES;   //点击蒙版popover消失， 默认YES
}



@end
