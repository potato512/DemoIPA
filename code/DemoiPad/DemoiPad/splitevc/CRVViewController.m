//
//  CRVViewController.m
//  DemoiPad
//
//  Created by zhangshaoyu on 2018/7/11.
//  Copyright © 2018年 zhangshaoyu. All rights reserved.
//

#import "CRVViewController.h"

@interface CRVViewController ()

@end

@implementation CRVViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"CRV汽车";
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(200.0, 200.0, (self.view.frame.size.width - 200.0 * 2), (self.view.frame.size.height - 200.0 * 2))];
    [self.view addSubview:imageview];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    imageview.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.3];
    imageview.image = [UIImage imageNamed:@"CRV"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
}

@end
