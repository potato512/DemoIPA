//
//  XianDaiViewController.m
//  DemoiPad
//
//  Created by zhangshaoyu on 2018/7/11.
//  Copyright © 2018年 zhangshaoyu. All rights reserved.
//

#import "XianDaiViewController.h"

@interface XianDaiViewController ()

@end

@implementation XianDaiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"北京现代达观汽车";
    
    CGFloat width = (self.view.frame.size.width - 180.0 - 200.0 * 2);
    CGFloat height = (self.view.frame.size.height - 200.0 * 2);
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(200.0, 200.0, width, height)];
    [self.view addSubview:imageview];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    imageview.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.3];
    imageview.image = [UIImage imageNamed:@"DaGuan"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor purpleColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
}

@end
