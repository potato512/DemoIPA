//
//  SpliteViewController.m
//  DemoiPad
//
//  Created by zhangshaoyu on 2018/7/11.
//  Copyright © 2018年 zhangshaoyu. All rights reserved.
//

#import "SpliteViewController.h"
#import "MainViewController.h"
#import "BeiChiViewController.h"
#import "CRVViewController.h"
#import "XianDaiViewController.h"
#import "LeiKeSaSiViewController.h"


@interface SpliteViewController () <UISplitViewControllerDelegate>

@end

@implementation SpliteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self setController];
    [self setSpliteController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setController
{
    //
    CGFloat originXY = 120.0f;
    //
    MainViewController *menuVC = [[MainViewController alloc] init];
//    UINavigationController *menuNav = [[UINavigationController alloc] initWithRootViewController:menuVC];
    BeiChiViewController *beiChiVC = [[BeiChiViewController alloc] init];
    UINavigationController *BeiChiNav = [[UINavigationController alloc] initWithRootViewController:beiChiVC];
    CRVViewController *crvVC = [[CRVViewController alloc] init];
    UINavigationController *crvNav = [[UINavigationController alloc] initWithRootViewController:crvVC];
    XianDaiViewController *xianDaiVC = [[XianDaiViewController alloc] init];
    UINavigationController *xianDaiNav = [[UINavigationController alloc] initWithRootViewController:xianDaiVC];
    LeiKeSaSiViewController *leiKeSaSiVC = [[LeiKeSaSiViewController alloc] init];
    UINavigationController *leiKeSaSiNav = [[UINavigationController alloc] initWithRootViewController:leiKeSaSiVC];
    //
    UISplitViewController *splitController = [[UISplitViewController alloc] init];
    splitController.delegate = self;
    splitController.minimumPrimaryColumnWidth = originXY;
    splitController.maximumPrimaryColumnWidth = originXY;
    splitController.view.backgroundColor = [UIColor orangeColor];
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    //
//    splitController.viewControllers = @[menuNav, BeiChiNav, crvNav, xianDaiNav, leiKeSaSiNav];
    splitController.viewControllers = @[menuVC, BeiChiNav, crvNav, xianDaiNav, leiKeSaSiNav];
    [self addChildViewController:splitController];
    [self.view addSubview:splitController.view];
    //
    UIView *coverView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, originXY, splitController.view.frame.size.height)];
    coverView.backgroundColor = [UIColor clearColor];
    __block UIView *lastView = nil;
    NSArray *items = @[@"main", @"detail", @"red", @"green"];
    [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *butotn = [UIButton buttonWithType:UIButtonTypeCustom];
        [coverView addSubview:butotn];
        butotn.tag = idx;
        butotn.frame = CGRectMake(0.0, (lastView.frame.origin.y + lastView.frame.size.height), coverView.frame.size.width, 60.0);
        [butotn setTitle:(NSString *)obj forState:UIControlStateNormal];
        [butotn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [butotn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
        [butotn addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
        
        lastView = butotn;
    }];
    [splitController.view addSubview:coverView];
}

- (void)setSpliteController
{
    //
    CGFloat originXY = 180.0f;
    //
    MainViewController *menuVC = [[MainViewController alloc] init];
    UINavigationController *menuNav = [[UINavigationController alloc] initWithRootViewController:menuVC];
    BeiChiViewController *beiChiVC = [[BeiChiViewController alloc] init];
    UINavigationController *BeiChiNav = [[UINavigationController alloc] initWithRootViewController:beiChiVC];
    CRVViewController *crvVC = [[CRVViewController alloc] init];
    UINavigationController *crvNav = [[UINavigationController alloc] initWithRootViewController:crvVC];
    XianDaiViewController *xianDaiVC = [[XianDaiViewController alloc] init];
    UINavigationController *xianDaiNav = [[UINavigationController alloc] initWithRootViewController:xianDaiVC];
    LeiKeSaSiViewController *leiKeSaSiVC = [[LeiKeSaSiViewController alloc] init];
    UINavigationController *leiKeSaSiNav = [[UINavigationController alloc] initWithRootViewController:leiKeSaSiVC];
    //
    self.delegate = self;
    self.minimumPrimaryColumnWidth = originXY;
    self.maximumPrimaryColumnWidth = originXY;
    self.view.backgroundColor = [UIColor orangeColor];
    /*
     UISplitViewControllerDisplayModeAutomatic,
     UISplitViewControllerDisplayModePrimaryHidden,
     UISplitViewControllerDisplayModeAllVisible,
     UISplitViewControllerDisplayModePrimaryOverlay,
    */
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModePrimaryOverlay;
    //
    NSArray *controllers = @[menuNav, BeiChiNav, crvNav, xianDaiNav, leiKeSaSiNav];
//    NSArray *controllers = @[menuVC, BeiChiNav, crvNav, xianDaiNav, leiKeSaSiNav];
    self.viewControllers = controllers;
//    [self addChildViewController:self];
//    [self.view addSubview:self.view];
    //
//    UIView *coverView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, originXY, self.view.frame.size.height)];
//    coverView.backgroundColor = [UIColor clearColor];
//    __block UIView *lastView = nil;
//    NSArray *items = @[@"main", @"detail", @"red", @"green"];
//    [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        UIButton *butotn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [coverView addSubview:butotn];
//        butotn.tag = idx;
//        butotn.frame = CGRectMake(0.0, (lastView.frame.origin.y + lastView.frame.size.height), coverView.frame.size.width, 60.0);
//        [butotn setTitle:(NSString *)obj forState:UIControlStateNormal];
//        [butotn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [butotn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
//        [butotn addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
//
//        lastView = butotn;
//    }];
//    [self.view addSubview:coverView];
    //
    SpliteViewController __weak *weakSelf = self;
    menuVC.itemClick = ^(NSInteger index) {
        UINavigationController *nextController = controllers[index + 1];
        [weakSelf showDetailViewController:nextController sender:nil];
    };
}

- (void)itemClick:(UIButton *)button
{
    NSInteger index = button.tag;
    NSLog(@"click %ld", index);
}

// UISplitViewControllerDelegate

- (BOOL)splitViewController:(UISplitViewController *)splitViewController showViewController:(UIViewController *)vc sender:(id)sender
{
    return YES;
}

@end
