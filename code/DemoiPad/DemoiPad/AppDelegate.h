//
//  AppDelegate.h
//  DemoiPad
//
//  Created by zhangshaoyu on 2018/7/9.
//  Copyright © 2018年 zhangshaoyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end


/*
 UIPopoverPresentationController
 
 
 UISplitViewController
 
 大多数时候，iPhone、iPod 应用与 iPad 应用开发没有太大的区别，但是 iPad 的屏幕比 iPhone 大，设计程序时可以充分利用 iPad 的大屏幕特点，例如 TabBar 和 Navigation 的使用会减少，相应的会采用新的一种 ViewController 来代替，那就是 UISplitViewController，这个控件是 iPad 专用的视图控制器。使用 SplitViewController 导航时，横屏情况下，左边显示一个导航表，点击导航项时右边显示对应的详情。横屏情况下显示方式会有所不同，默认只显示详情面板，原来左侧的导航列表会通过浮动窗口隐藏，需要从边缘向内侧拖动来显示。
 在 iPhone 应用中，使用导航控制器由上一层界面进入下一层界面，在下一层界面处理完成后，用户可以非常方便的返回上一层界面。这种方式在 iPhone 应用里非常方便，因为小屏幕通常只能显示一个界面，但对于 iPad 来说，那么大的屏幕只显示一个列表会显得不太好看，同时用户操作也不方便，所以在 iPad 中，通常使用 SplitViewController 来实现导航。iPhone 和 iPad 的系统设置就是典型的按设备区别界面的应用。
 
 
 */
