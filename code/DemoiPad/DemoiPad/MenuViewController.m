//
//  MenuViewController.m
//  DemoiPad
//
//  Created by zhangshaoyu on 2018/7/9.
//  Copyright © 2018年 zhangshaoyu. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *array;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 注意：如果内容控制器里设置了 preferredContentSize 大小 self.preferredContentSize = CGSizeMake(200, 200); 那么 self.popOver.popoverContentSize 将无意义，而且在实际开发中也应该由内容控制器控制大小
//    self.view.backgroundColor = [UIColor redColor];
//    self.preferredContentSize = CGSizeMake(200, 200);
    
    
    UITableView *tableview = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:tableview];
    tableview.delegate = self;
    tableview.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)];
        [cell.contentView addSubview:label];
        label.tag = 10000;
        label.textColor = [UIColor blackColor];
    }
    UILabel *label = [cell.contentView viewWithTag:10000];
    label.text = self.array[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.itemClick) {
        self.itemClick(indexPath.row);
    }
}

- (NSArray *)array
{
    if (_array == nil) {
        _array = @[@"深圳", @"广州", @"珠海", @"中山", @"东莞", @"惠州", @"河源", @"梅州", @"潮州", @"肇庆", @"阳江"];
    }
    return _array;
}

@end
