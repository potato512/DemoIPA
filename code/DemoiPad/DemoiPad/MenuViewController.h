//
//  MenuViewController.h
//  DemoiPad
//
//  Created by zhangshaoyu on 2018/7/9.
//  Copyright © 2018年 zhangshaoyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController

@property (nonatomic, copy) void (^itemClick)(NSInteger index);

@end
